var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

var webpack = require('webpack');
var path = require('path');
module.exports = {
  context :  path.resolve(path.join(__dirname,'/public/js')),
  entry : './core.js',
  output: {
      path: __dirname + '/public/dist',
      filename: 'bundle.js'
  },
  resolve :{
    modulesDirectories: ["web_modules", "node_modules", "bower_components"],
    root: path.resolve(path.join(__dirname,'/public/js')),
    alias : {
      todos : 'services/todos',
      todoService : 'services/todoService',
      todoController : 'controllers/todoController',
      mainController : 'controllers/mainController'
    }
  },
  plugins: [

      new ngAnnotatePlugin({
          add: true
      }),
      new webpack.optimize.UglifyJsPlugin({
          compress: {
              warnings: false
          }
      })
  ],
  loaders: [
      {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
      },
      {   test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
          test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file'
      },
      {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&mimetype=image/svg+xml'
      },
      {
          test: /\.js?$/,
          include: `${__dirname}/public/js`,
          loader: 'babel'
      }
  ],
  preLoaders: [
      {
          test: /\.js?$/,
          exclude: /node_modules/,
          loader: 'jshint'
      }
  ]
};
